import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigurationService } from '../services/configuration.service';

@Injectable()
export class ConfigurationGuard implements CanActivate {
	constructor(
		private router: Router,
		private configurationService: ConfigurationService
	) {}

	public canActivate(): Observable<boolean> {
		return this.configurationService.getConfiguracion().pipe(
			map(configuracion => {
				if (configuracion?.permitirRegistro!) {
					return true;
				} else {
					this.router.navigate(['/login']);
					return false;
				}
			})
		);
	}
}
