import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SigninComponent } from './components/signin/signin.component';
import { TableroComponent } from './components/tablero/tablero.component';
import { AuthGuard } from './guards/auth.guard';
import { ConfigurationGuard } from './guards/configuration.guard';

const routes: Routes = [
	{ path: '', component: TableroComponent, canActivate: [AuthGuard] },
	{ path: 'login', component: LoginComponent },
	{
		path: 'signin',
		component: SigninComponent,
		canActivate: [ConfigurationGuard]
	},
	{
		path: 'configuracion',
		component: ConfiguracionComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'cliente/editar/:id',
		component: EditarClienteComponent,
		canActivate: [AuthGuard]
	},
	{ path: '**', component: NotFoundComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
