import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabeceroComponent } from './components/cabecero/cabecero.component';
import { TableroComponent } from './components/tablero/tablero.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';
import { LoginComponent } from './components/login/login.component';
import { SigninComponent } from './components/signin/signin.component';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PiePaginaComponent } from './components/pie-pagina/pie-pagina.component';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { ClientesService } from './services/clientes.service';
import { LoginService } from './services/login.service';
import { AuthGuard } from './guards/auth.guard';
import { ConfigurationService } from './services/configuration.service';
import { ConfigurationGuard } from './guards/configuration.guard';

@NgModule({
	declarations: [
		AppComponent,
		CabeceroComponent,
		TableroComponent,
		ClientesComponent,
		EditarClienteComponent,
		LoginComponent,
		SigninComponent,
		ConfiguracionComponent,
		NotFoundComponent,
		PiePaginaComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		AngularFireModule.initializeApp(
			environment.firestore,
			'control-clientes'
		),
		AngularFirestoreModule,
		AngularFireAuthModule,
		FlashMessagesModule.forRoot()
	],
	providers: [
		ClientesService,
		LoginService,
		AuthGuard,
		ConfigurationService,
		ConfigurationGuard
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
