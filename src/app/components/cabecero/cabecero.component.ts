import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
	selector: 'app-cabecero',
	templateUrl: './cabecero.component.html',
	styleUrls: ['./cabecero.component.css']
})
export class CabeceroComponent implements OnInit {
	public isLoggedIn: boolean = false;
	public logggedInUser: string = '';
	public permitirRegistro: boolean = false;

	constructor(
		private loginService: LoginService,
		private router: Router,
		private configurationService: ConfigurationService
	) {}

	ngOnInit(): void {
		this.loginService.getAuth().subscribe(auth => {
			if (auth) {
				this.isLoggedIn = true;
				this.logggedInUser = auth.email!;
			} else {
				this.isLoggedIn = false;
			}
		});
		this.configurationService
			.getConfiguracion()
			.subscribe(configuracion => {
				this.permitirRegistro = configuracion?.permitirRegistro!;
			});
	}

	public logout(): void {
		this.loginService.logout();
		this.isLoggedIn = false;
		this.router.navigate(['/login']);
	}
}
