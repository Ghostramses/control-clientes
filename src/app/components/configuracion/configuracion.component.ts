import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Configuration } from 'src/app/models/configuation.model';
import { ConfigurationService } from 'src/app/services/configuration.service';

@Component({
	selector: 'app-configuracion',
	templateUrl: './configuracion.component.html',
	styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {
	public permitirRegistro: boolean = false;

	constructor(
		private router: Router,
		private configurationService: ConfigurationService
	) {}

	ngOnInit(): void {
		this.configurationService
			.getConfiguracion()
			.subscribe((configuracion: Configuration | undefined) => {
				this.permitirRegistro = configuracion?.permitirRegistro!;
			});
	}

	public guardar(): void {
		let configuracion: Configuration = {
			permitirRegistro: this.permitirRegistro
		};
		this.configurationService.modificarConfiguracion(configuracion);
		this.router.navigate(['/']);
	}
}
