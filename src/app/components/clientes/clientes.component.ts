import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Cliente } from 'src/app/models/cliente.model';
import { ClientesService } from 'src/app/services/clientes.service';

@Component({
	selector: 'app-clientes',
	templateUrl: './clientes.component.html',
	styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
	@ViewChild('clienteForm') clienteForm!: NgForm;
	@ViewChild('botonCerrar') botonCerrar!: ElementRef;

	public clientes: Cliente[] = [];
	public cliente: Cliente = {
		nombre: '',
		apellido: '',
		email: '',
		saldo: 0
	};

	constructor(
		private clientesServicio: ClientesService,
		private flashMessages: FlashMessagesService
	) {}

	public ngOnInit(): void {
		this.clientesServicio
			.getClientes()
			.subscribe(clientes => (this.clientes = clientes));
	}

	public getSaldoTotal(): number {
		let total: number = 0;
		if (this.clientes) {
			this.clientes.forEach(cliente => (total += cliente.saldo!));
		}
		return total;
	}

	public agregar({
		value,
		valid
	}: {
		value: Cliente;
		valid: boolean | null;
	}): void {
		if (!valid) {
			this.flashMessages.show(
				'Por favor llena el formulario correctamente',
				{ cssClass: 'alert-danger', timeout: 4000 }
			);
		} else {
			this.clientesServicio.agregarCliente(value);
			this.clienteForm.resetForm();
			this.cerrarModal();
		}
	}

	private cerrarModal(): void {
		this.botonCerrar.nativeElement.click();
	}
}
