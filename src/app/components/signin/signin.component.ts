import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { LoginService } from 'src/app/services/login.service';

@Component({
	selector: 'app-signin',
	templateUrl: './signin.component.html',
	styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
	public email: string = '';
	public password: string = '';

	constructor(
		private router: Router,
		private flashMessages: FlashMessagesService,
		private loginService: LoginService
	) {}

	ngOnInit(): void {
		this.loginService.getAuth().subscribe(auth => {
			if (auth) {
				this.router.navigate(['/']);
			}
		});
	}

	public signin(): void {
		this.loginService
			.signin(this.email, this.password)
			.then(res => this.router.navigate(['/']))
			.catch(error =>
				this.flashMessages.show(error.message, {
					cssClass: 'alert-danger container',
					timeout: 4000
				})
			);
	}
}
