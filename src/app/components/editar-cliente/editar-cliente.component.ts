import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Cliente } from 'src/app/models/cliente.model';
import { ClientesService } from 'src/app/services/clientes.service';

@Component({
	selector: 'app-editar-cliente',
	templateUrl: './editar-cliente.component.html',
	styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {
	public cliente: Cliente = {
		nombre: '',
		apellido: '',
		email: '',
		saldo: 0
	};

	private id: string = '';

	constructor(
		private clientesServicio: ClientesService,
		private flashMessages: FlashMessagesService,
		private router: Router,
		private route: ActivatedRoute
	) {}

	ngOnInit(): void {
		this.id = this.route.snapshot.params['id'];
		this.clientesServicio
			.getCliente(this.id)
			.subscribe(cliente => (this.cliente = cliente!));
	}

	public guardar({
		value,
		valid
	}: {
		value: Cliente;
		valid: boolean | null;
	}): void {
		if (!valid) {
			this.flashMessages.show(
				'Por favor llena el formulario correctamente',
				{
					cssClass: 'alert-danger',
					timeout: 4000
				}
			);
		} else {
			value.id = this.id;
			this.clientesServicio.modificarCliente(value);
			this.router.navigate(['/']);
		}
	}

	public eliminar(): void {
		if (confirm('¿Seguro que desea eliminar el cliente?')) {
			this.clientesServicio.eliminarCliente(this.cliente);
			this.router.navigate(['/']);
		}
	}
}
