import { Injectable } from '@angular/core';
import {
	AngularFirestore,
	AngularFirestoreCollection,
	AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Cliente } from '../models/cliente.model';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class ClientesService {
	public clientesColeccion: AngularFirestoreCollection<Cliente>;
	public clienteDoc: AngularFirestoreDocument<Cliente> | null = null;
	public clientes: Observable<Cliente[]> = new Observable<Cliente[]>();
	public cliente: Observable<Cliente | null> = new Observable<Cliente>();

	constructor(private db: AngularFirestore) {
		this.clientesColeccion = db.collection('clientes', ref =>
			ref.orderBy('nombre', 'asc')
		);
	}

	public getClientes(): Observable<Cliente[]> {
		// Obtener los clientes
		this.clientes = this.clientesColeccion.snapshotChanges().pipe(
			map(cambios =>
				cambios.map(accion => {
					const datos = accion.payload.doc.data() as Cliente;
					datos.id = accion.payload.doc.id;
					return datos;
				})
			)
		);
		return this.clientes;
	}

	public agregarCliente(cliente: Cliente): void {
		this.clientesColeccion.add(cliente);
	}

	public getCliente(id: string): Observable<Cliente | null> {
		this.clienteDoc = this.db.doc<Cliente>(`clientes/${id}`);
		this.cliente = this.clienteDoc.snapshotChanges().pipe(
			map(accion => {
				if (accion.payload.exists === false) {
					return null;
				} else {
					const datos = accion.payload.data() as Cliente;
					datos.id = accion.payload.id;
					return datos;
				}
			})
		);
		return this.cliente;
	}

	public modificarCliente(cliente: Cliente): void {
		this.clienteDoc = this.db.doc(`clientes/${cliente.id}`);
		this.clienteDoc.update(cliente);
	}

	public eliminarCliente(cliente: Cliente): void {
		this.clienteDoc = this.db.doc(`clientes/${cliente.id}`);
		this.clienteDoc.delete();
	}
}
