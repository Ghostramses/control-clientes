import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';

@Injectable()
export class LoginService {
	constructor(private authService: AngularFireAuth) {}

	public login(email: string, password: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.authService.signInWithEmailAndPassword(email, password).then(
				datos => resolve(datos),
				error => reject(error)
			);
		});
	}

	public getAuth() {
		return this.authService.authState.pipe(map(auth => auth));
	}

	public logout(): void {
		this.authService.signOut();
		console.log('desloggeado');
	}

	public signin(email: string, password: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.authService
				.createUserWithEmailAndPassword(email, password)
				.then(
					datos => resolve(datos),
					error => reject(error)
				);
		});
	}
}
