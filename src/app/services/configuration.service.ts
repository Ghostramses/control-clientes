import { Injectable } from '@angular/core';
import {
	AngularFirestore,
	AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Configuration } from '../models/configuation.model';

@Injectable()
export class ConfigurationService {
	private configuracionDoc: AngularFirestoreDocument<Configuration> | null = null;
	private configuracion: Observable<
		Configuration | undefined
	> = new Observable<Configuration | undefined>();

	private id = '1';

	constructor(private db: AngularFirestore) {}

	public getConfiguracion(): Observable<Configuration | undefined> {
		this.configuracionDoc = this.db.doc<Configuration>(
			`configuracion/${this.id}`
		);
		this.configuracion = this.configuracionDoc.valueChanges();
		return this.configuracion;
	}

	public modificarConfiguracion(configuracion: Configuration) {
		this.configuracionDoc = this.db.doc<Configuration>(
			`configuracion/${this.id}`
		);
		this.configuracionDoc.update(configuracion);
	}
}
