// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	firestore: {
		apiKey: 'AIzaSyBNhaj76p-uJl5CFihFEiSedyRiMZPaToM',
		authDomain: 'control-clientes-e51f6.firebaseapp.com',
		projectId: 'control-clientes-e51f6',
		storageBucket: 'control-clientes-e51f6.appspot.com',
		messagingSenderId: '386473273606',
		appId: '1:386473273606:web:4d6b58dfeaa0725e509687'
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
